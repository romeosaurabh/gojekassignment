package com.example.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class MainFragment extends Fragment {
    private static final String TAG = "MainFragment";

    public static final String PREFERENCE = "pref";
    public static final String TIME = "timeKey";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private Activity mActivity;
    private TrendingDataAdapter mTrendingDataAdapter;
    private TextView mRetryButton;
    private ImageView mRetryImageView;
    private ShimmerFrameLayout mShimmerViewContainer;
    private SQLiteDatabaseHandler mSqLiteDatabaseHandler;
    private SharedPreferences mSharedPreferences;

    public interface ApiService {
        @GET("repositories/")
        Call<List<TrendData>> getTrendData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mActivity = getActivity();
        mSqLiteDatabaseHandler = new SQLiteDatabaseHandler(mActivity);
        mSharedPreferences = mActivity.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    private void getListData() {
        boolean fromDataBase = ((mSqLiteDatabaseHandler != null) && (mSqLiteDatabaseHandler.getAllTrendsData().size() > 0));
        long savedTime = mSharedPreferences.getLong(TIME, -1);
        long currentTime = System.currentTimeMillis();
        int hours = (int) ((Math.abs(savedTime - currentTime) / (1000 * 60 * 60)) % 24);

        if (savedTime == -1 || hours > 2 || !fromDataBase) {
            getDataFromServer(false);
        } else {
            getDataFromDB();
        }
    }

    private void getDataFromDB() {
        mTrendingDataAdapter.clearTrendingItems();
        mTrendingDataAdapter.addTrendingItems(mSqLiteDatabaseHandler.getAllTrendsData());
        setLayoutVisibility(false);
    }

    private void getDataFromServer(final boolean isSwipeTofresh) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://github-trending-api.now.sh/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        ApiService apiService = retrofit.create(ApiService.class);
        Call<List<TrendData>> listCall = apiService.getTrendData();

        listCall.enqueue(new Callback<List<TrendData>>() {
            @Override
            public void onResponse(Call<List<TrendData>> call, Response<List<TrendData>> response) {
                List<TrendData> trendDataList = response.body();
                addDataToDBAndInflateView(trendDataList, true, isSwipeTofresh);
                Log.d(TAG, "onResponse called");
            }

            @Override
            public void onFailure(Call<List<TrendData>> call, Throwable t) {
                Log.d(TAG, "onFailure");
                addDataToDBAndInflateView(new ArrayList<TrendData>(), false, isSwipeTofresh);
            }
        });
    }

    private void addDataToDBAndInflateView(List<TrendData> trendDataList, boolean response, boolean isSwipeTofresh) {
        if (response) {
            if (mSqLiteDatabaseHandler != null) {
                mSqLiteDatabaseHandler.deleteTrendTable();
            }
            for (TrendData trendData : trendDataList) {
                mSqLiteDatabaseHandler.addTrendData(trendData);
            }
            mTrendingDataAdapter.clearTrendingItems();
            mTrendingDataAdapter.addTrendingItems(mSqLiteDatabaseHandler.getAllTrendsData());
            setLayoutVisibility(false);
        } else {
            if (!isSwipeTofresh) {
                mTrendingDataAdapter.addTrendingItems(trendDataList);
                setLayoutVisibility(true);
            }
        }
    }

    private void setLayoutVisibility(boolean retryVisible) {
        mShimmerViewContainer.stopShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.GONE);
        if (retryVisible) {
            mSwipeRefreshLayout.setVisibility(View.GONE);
            mRetryImageView.setVisibility(View.VISIBLE);
            mRetryButton.setVisibility(View.VISIBLE);
        } else {
            mRetryImageView.setVisibility(View.GONE);
            mRetryButton.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
    }

    private boolean isInternetConnectionAvailable() {
        ConnectivityManager cm = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view, container, false);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mRetryButton = view.findViewById(R.id.retryButton);
        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetConnectionAvailable()) {
                    getDataFromServer(false);
                } else {
                    Toast.makeText(mActivity, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mRetryImageView = view.findViewById(R.id.noInternetImage);
        mTrendingDataAdapter = new TrendingDataAdapter(new ArrayList<TrendData>());
        mSwipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        mRecyclerView = view.findViewById(R.id.rvItems);
        mLinearLayoutManager = new LinearLayoutManager(mActivity);
        mLinearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDataFromServer(true);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 100);
            }
        });

        getListData();
        mRecyclerView.setAdapter(mTrendingDataAdapter);

        return view;
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putLong(TIME, System.currentTimeMillis()).apply();
        super.onPause();
    }
}
