package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

public class TrendData {

	@SerializedName("forks")
	private int forks;

	@SerializedName("author")
	private String author;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("language")
	private String language;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("languageColor")
	private String languageColor;

	@SerializedName("stars")
	private int stars;

	private boolean expanded;

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public void setForks(int forks){
		this.forks = forks;
	}

	public int getForks(){
		return forks;
	}

	public void setAuthor(String author){
		this.author = author;
	}

	public String getAuthor(){
		return author;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setLanguage(String language){
		this.language = language;
	}

	public String getLanguage(){
		return language;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setLanguageColor(String languageColor){
		this.languageColor = languageColor;
	}

	public String getLanguageColor(){
		return languageColor;
	}

	public void setStars(int stars){
		this.stars = stars;
	}

	public int getStars(){
		return stars;
	}

	@Override
 	public String toString(){
		return 
			"TrendData{" +
			"forks = '" + forks + '\'' + 
			",author = '" + author + '\'' +
			",name = '" + name + '\'' + 
			",description = '" + description + '\'' + 
			",language = '" + language + '\'' + 
			",avatar = '" + avatar + '\'' + 
			",languageColor = '" + languageColor + '\'' + 
			",stars = '" + stars + '\'' + 
			"}";
		}
}