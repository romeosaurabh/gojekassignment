package com.example.myapplication;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class TrendingDataAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<TrendData> mTrendingDataList;

    public TrendingDataAdapter(List<TrendData> trendingData) {
        mTrendingDataList = trendingData;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, final int position) {
        holder.onBind(position);

        holder.itemView.findViewById(R.id.card_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean expanded = mTrendingDataList.get(position).isExpanded();
                mTrendingDataList.get(position).setExpanded(!expanded);
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        int itemCount = 0;
        if (mTrendingDataList != null) {
            itemCount = mTrendingDataList.size();
        }
        return itemCount;
    }

    public void addTrendingItems(List<TrendData> trendingData) {
        mTrendingDataList.addAll(trendingData);
        notifyDataSetChanged();
    }

    public void clearTrendingItems() {
        mTrendingDataList.clear();
    }

    class ViewHolder extends BaseViewHolder {
        private TextView titleTextView;
        private TextView subTitleTextView;
        private TextView descriptionTextView;
        private TextView languageTextView;
        private TextView starsTextView;
        private TextView forksTextView;
        private ImageView coverImageView;
        private View subItem;


        public ViewHolder(View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.title);
            subTitleTextView = itemView.findViewById(R.id.subtitle);
            subItem = itemView.findViewById(R.id.subHeader);
            descriptionTextView = itemView.findViewById(R.id.description);
            languageTextView = itemView.findViewById(R.id.language);
            starsTextView = itemView.findViewById(R.id.stars);
            forksTextView = itemView.findViewById(R.id.forks);
            coverImageView = itemView.findViewById(R.id.thumbnail);
        }

        @Override
        protected void clear() {
            coverImageView.setImageDrawable(null);
            titleTextView.setText("");
            subTitleTextView.setText("");
            descriptionTextView.setText("");
            languageTextView.setText("");
            starsTextView.setText("");
            forksTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final TrendData trendingData = mTrendingDataList.get(position);

            if (trendingData.getAvatar() != null) {
                Glide.with(itemView.getContext())
                        .load(trendingData.getAvatar())
                        .into(coverImageView);
            }

            if (trendingData.getAuthor() != null) {
                titleTextView.setText(trendingData.getAuthor());
            }

            if (trendingData.getName() != null) {
                subTitleTextView.setText(trendingData.getName());
            }

            subItem.setVisibility(trendingData.isExpanded() ? View.VISIBLE : View.GONE);

            if (trendingData.getDescription() != null) {
                if (trendingData.getDescription().length() > 0) {
                    descriptionTextView.setVisibility(View.VISIBLE);
                    descriptionTextView.setText(trendingData.getDescription());
                } else {
                    descriptionTextView.setVisibility(View.GONE);
                }
            }

            if (trendingData.getLanguage() != null) {
                if (trendingData.getLanguage().length() > 0) {
                    languageTextView.setVisibility(View.VISIBLE);
                    languageTextView.setText(trendingData.getLanguage());
                    Drawable[] drawables = languageTextView.getCompoundDrawables();
                    if (drawables[0] != null) {
                        int color = trendingData.getLanguageColor() != null ? Color.parseColor(trendingData.getLanguageColor()) : Color.parseColor("#000000");
                        drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    languageTextView.setVisibility(View.INVISIBLE);
                }
            }

            starsTextView.setText(String.valueOf(trendingData.getStars()));
            forksTextView.setText(String.valueOf(trendingData.getForks()));
        }
    }
}
