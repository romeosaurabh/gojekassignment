package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {
    private static final String TAG = "SQLiteDatabaseHandler";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TrendsDB";
    private static final String TABLE_NAME = "Trends";
    private static final String KEY_NAME = "name";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_DESC = "description";
    private static final String KEY_LANGUAGE = "language";
    private static final String KEY_LANGUAGE_COLOR = "languageColor";
    private static final String KEY_STARS = "stars";
    private static final String KEY_FORKS = "forks";
    private static final String KEY_IMAGE_URL = "imageUrl";

    private static final String[] COLUMNS = {KEY_NAME, KEY_AUTHOR, KEY_DESC, KEY_LANGUAGE, KEY_LANGUAGE_COLOR, KEY_STARS, KEY_FORKS, KEY_IMAGE_URL};

    public SQLiteDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "oncreate");
        String CREATION_TABLE = "CREATE TABLE " + TABLE_NAME + "( "
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, " + "name TEXT, "
                + "author TEXT, " + "description TEXT," + "language TEXT,"
                + "languageColor TEXT," + "stars INTEGER," + "forks INTEGER,"
                + "imageUrl TEXT )";

        db.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade");
        // you can implement here migration process
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }

    public List<TrendData> getAllTrendsData() {
        Log.d(TAG, "getAllTrendsData");
        List<TrendData> trendDataLinkedList = new LinkedList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(query, null);
            TrendData trendData;

            if (cursor.moveToFirst()) {
                do {
                    trendData = new TrendData();
                    trendData.setName(cursor.getString(1));
                    trendData.setAuthor(cursor.getString(2));
                    trendData.setDescription(cursor.getString(3));
                    trendData.setLanguage(cursor.getString(4));
                    trendData.setLanguageColor(cursor.getString(5));
                    trendData.setStars(Integer.parseInt(cursor.getString(6)));
                    trendData.setForks(Integer.parseInt(cursor.getString(7)));
                    trendData.setAvatar(cursor.getString(8));
                    trendDataLinkedList.add(trendData);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException ex) {
            Log.e(TAG, "exception : " + ex.toString());
        }

        return trendDataLinkedList;
    }

    public void addTrendData(TrendData trendData) {
        Log.d(TAG, "addTrendData");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, trendData.getName());
        values.put(KEY_AUTHOR, trendData.getAuthor());
        values.put(KEY_DESC, trendData.getDescription());
        values.put(KEY_LANGUAGE, trendData.getLanguage());
        values.put(KEY_LANGUAGE_COLOR, trendData.getLanguageColor());
        values.put(KEY_STARS, trendData.getStars());
        values.put(KEY_FORKS, trendData.getForks());
        values.put(KEY_IMAGE_URL, trendData.getAvatar());
        // insert
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void deleteTrendTable() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from " + TABLE_NAME);
            db.close();
        } catch (SQLiteException ex) {
            Log.e(TAG, "exception : " + ex.toString());
        }
    }

}
